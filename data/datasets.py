import anndata as ad
import numpy as np
import scanpy as sc

class MousePancreas():
    '''
    Data used in Deep feature extraction of single-cell transcriptomes by
    generative adversarial network, Bahrami et al, 2020
    '''

    def __init__(self, path=None):
        # we expect a csv file
        if path is None:
            self.path = 'data/files/GSM2230761_mouse1_umifm_counts.csv'
        else:
            self.path = path

    def get_adata(self, subsample=None, preprocessing=True):
        '''
        if not None, subsample is the rate of obs to keep
        '''
        counts_csv = np.genfromtxt(self.path, delimiter=',', dtype=None,
            encoding=None)

        adata = ad.AnnData(counts_csv[1:, 3:].astype(np.float32))
        # each row corresponds to a cell with a barcode, and each column
        # corresponds to a gene with a gene id

        adata.obs_names = counts_csv[1:, 1].astype(str)
        adata.var_names = counts_csv[0, 3:].astype(str)
        adata.obs["cell_type"] = counts_csv[1:, 2].astype(str)
        adata.obs_names_make_unique()

        if preprocessing:
            # the most basic preprocessing as seen in
            # https://scanpy-tutorials.readthedocs.io/en/latest/pbmc3k.html
            sc.pp.filter_cells(adata, min_genes=300)
            sc.pp.filter_genes(adata, min_cells=5)

        if subsample is not None:
            sc.pp.subsample(adata, n_obs=int(subsample * adata.n_obs),
                copy=False) 

        return adata

class PBMC():
    '''
    Data used in scVAE: Variational auto-encoders for single-cell gene
    expression data, Christopher H Gronbech et al, 2020

    It consists in the fusion of 9 different PMBC purified cell types
    '''

    def __init__(self, path_list=None):
        # expect a list of paths to directories
        if path_list is None:
            self.common_path = 'data/files/10x_pbmc_pp/original/'
            self.common_suffix = 'filtered_matrices_mex/hg19'
            self.path_list = ['10x_pbmc_pp-all-cd4+_cd25+_regulatory_t_cells/',
            '10x_pbmc_pp-all-cd4+_cd45ra+_cd25__naïve_t_cells/',
            '10x_pbmc_pp-all-cd4+_cd45ro+_memory_t_cells/',
            '10x_pbmc_pp-all-cd4+_helper_t_cells/',
            '10x_pbmc_pp-all-cd8+_cd45ra+_naïve_cytotoxic_t_cells/',
            '10x_pbmc_pp-all-cd8+_cytotoxic_t_cells/',
            '10x_pbmc_pp-all-cd19+_b_cells/',
            '10x_pbmc_pp-all-cd34+_cells/',
            '10x_pbmc_pp-all-cd56+_natural_killer_cells/']
        else:
            self.path_list = path_list

    def get_adata(self, subsample=None, preprocessing=True):
        '''
        if not None, subsample is the rate of obs to keep
        '''
        adata_list = []
        dataset_length_list = []
        for d in self.path_list:
            adata_ = sc.read_10x_mtx(
                self.common_path + d + self.common_suffix, # the directory with the `.mtx` file
                cache=True, # write a cache file for faster subsequent reading
            )
            if subsample is not None:
                sc.pp.subsample(adata_, n_obs=int(subsample * adata_.n_obs),
                    copy=False) 
            adata_list.append(adata_)
            dataset_length_list.append(adata_.n_obs)
        adata = ad.concat(
            adata_list,
            axis=0, # concat on the obs (cell names) since we stack more cells
            join="outer", # we take the union of all the vars (gene names) 
        )
        # add the truth label
        lbls = np.concatenate([
            np.full(length, idx) for (idx, length) in enumerate(dataset_length_list)
        ])
        cell_types = ['regulatory T cells', 'naive T cells', 'memory T cells',
        'helper T cells', 'naive cytotoxic T cells', 'cytotoxic T cells',
        'B cells', 'cells', 'natural killer cells']
        adata.obs["label_int"] = lbls.astype(int)
        adata.obs["cell_type"] = np.asarray([cell_types[lbl] for lbl in lbls])

        if preprocessing:
            # the most basic preprocessing as seen in
            # https://scanpy-tutorials.readthedocs.io/en/latest/pbmc3k.html
            sc.pp.filter_cells(adata, min_genes=300)
            sc.pp.filter_genes(adata, min_cells=5)

        return adata
