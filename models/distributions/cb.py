import torch
import torch.nn as nn

class ContinuousBernoulli(nn.Module):
    '''
    Inherits from Module so that it has a forward function that is called when
    stacked into a nn.Sequential
    Does not have parameters on its own

    Parametrization following Loaiza's paper
    '''
    
    def __init__(self, base_output_dim):
        super().__init__()
        # base_output_dim is how many rv we have (on the output dim)
        self.base_output_dim = base_output_dim
        self.decoder_output_dim = base_output_dim # bc 1 param lambda_

    def forward(self, decoder_output):
        # transforms the decoder output into the real parameters for the NB
        return torch.sigmoid(decoder_output)

    def llkh(self, x):
        lambda_ = self.param

        eps = 1e-6
        def log_norm_const(x):
            # numerically stable computation
            x = torch.clamp(x, eps, 1 - eps)
            x = torch.where((x < 0.49) | (x > 0.51), x, 0.49 *
                    torch.ones_like(x))
            return torch.log((2 * self.tarctanh(1 - 2 * x)) /
                            (1 - 2 * x) + eps)
        
        return torch.sum(x * torch.log(lambda_ + eps) +
                            (1 - x) * torch.log(1 - lambda_ + eps) +
                            log_norm_const(lambda_), dim=(1))

    def mean(self):
        '''
        Compute the mean of a continuous Bernoulli distribution from
        '''
        lambda_ = torch.clamp(self.param, 10e-6, 1 - 10e-6)
        lambda_ = torch.where((lambda_ < 0.49) | (lambda_ > 0.51),
            lambda_, 0.49 *
            torch.ones_like(lambda_)
        )
        return lambda_ / (2 * lambda_ - 1) + 1 / (2 * self.tarctanh(1 - 2 * lambda_))

    def tarctanh(self, x):
        '''
        arctanh function in pytorch
        '''
        return 0.5 * torch.log((1 + x)/(1 - x))
