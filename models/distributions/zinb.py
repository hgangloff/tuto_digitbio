import torch
import torch.nn as nn

class ZeroInflatedNegativeBinomial(nn.Module):
    '''
    Inherits from Module so that it has a forward function that is called when
    stacked into a nn.Sequential
    Does not have parameters on its own

    We use Wikipedia Negative Binomial parametrization
    '''
    
    def __init__(self, base_output_dim):
        super().__init__()
        # base_output_dim is how many rv we have (on the output dim)
        self.base_output_dim = base_output_dim
        self.decoder_output_dim = base_output_dim * 3 # bc 3 param r, p and rho

    def forward(self, decoder_output):
        # transforms the decoder output into the real parameters for the NB
        log_r = decoder_output[:, :self.base_output_dim]
        p = torch.sigmoid(
            decoder_output[:, self.base_output_dim:2 * self.base_output_dim]
        )
        r = torch.exp(log_r)
        rho = torch.sigmoid(decoder_output[:, 2 * self.base_output_dim:])
        
        # Avoid problematic parameter values
        r = torch.clamp(r, min=1e-6, max=1e6)
        p = torch.clamp(p, min=1e-6, max=1 - 1e-6)
        rho = torch.clamp(rho, min=1e-6, max=1 - 1e-6)
        return r, p, rho

    def llkh(self, x):
        ''' dichotomy around eps adapted from 
        https://github.com/scverse/scvi-tools/blob/main/scvi/distributions/_negative_binomial.py
        with a different parametrization of the distribution
        '''
        r, p, rho = self.param 

        eps = 1e-8
        case_zero = torch.log(rho) + torch.nn.functional.softplus(
            torch.log(1 / rho - 1) + r * torch.log(p)
            )
        case_zero = torch.mul((x < eps).float(), case_zero)
        
        case_non_zero = (torch.log(1 - rho) + torch.lgamma(r + x) - (torch.lgamma(r) + torch.lgamma(x + 1))
                + r * torch.log(p) + x * torch.log(1 - p))
        case_non_zero = torch.mul((x >= eps).float(), case_non_zero)
        
        res = case_zero + case_non_zero

        return torch.sum(res, dim=(1))

    def mean(self):
        r, p, rho = self.param
        return r * (1 - p) / p * (1 - rho)
