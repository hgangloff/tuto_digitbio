import torch
import torch.nn as nn

class NegativeBinomial(nn.Module):
    '''
    Inherits from Module so that it has a forward function that is called when
    stacked into a nn.Sequential
    Does not have parameters on its own

    We use Wikipedia Negative Binomial parametrization
    '''
    
    def __init__(self, base_output_dim):
        super().__init__()
        # base_output_dim is how many rv we have (on the output dim)
        self.base_output_dim = base_output_dim
        self.decoder_output_dim = base_output_dim * 2 # bc 2 param r and p

    def forward(self, decoder_output):
        # transforms the decoder output into the real parameters for the NB
        log_r = decoder_output[:, :self.base_output_dim]
        p = torch.sigmoid(
            decoder_output[:, self.base_output_dim:2 * self.base_output_dim]
        )
        r = torch.exp(log_r)

        # Avoid problematic parameter values
        r = torch.clamp(r, min=1e-6, max=1e6)
        p = torch.clamp(p, min=1e-6, max=1 - 1e-6)
        return r, p

    def llkh(self, x):
        r, p = self.param
        return torch.sum(
                torch.lgamma(r + x) - (torch.lgamma(r) + torch.lgamma(x + 1))
                + r * torch.log(p) + x * torch.log(1 - p),
            dim=(1)
            )

    def mean(self):
        r, p = self.param
        return r * (1 - p) / p
