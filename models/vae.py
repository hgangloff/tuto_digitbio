import torch
import torch.nn as nn
from models.utils import MLP

class VAE(nn.Module):
    def __init__(self, input_dim, hidden_dims, latent_dim, pxz_distribution, beta=1):
        super().__init__()
        self.input_dim = input_dim
        self.hidden_dims = hidden_dims
        self.latent_dim = latent_dim
        self.beta = beta

        self.pxz_distribution = pxz_distribution(input_dim)
        
        self.q_z_x = MLP(
            self.input_dim,
            self.hidden_dims,
            2 * self.latent_dim
        )

        self.p_x_z = nn.Sequential(
            MLP(
                self.latent_dim,
                self.hidden_dims[::-1],
                self.pxz_distribution.decoder_output_dim
            ),
            self.pxz_distribution # call the forward function of the class
        )

    
    def reparameterize(self, mu, logvar):
        if self.training:
            std = torch.exp(torch.mul(logvar, 0.5))
            eps = torch.randn_like(std)
            return eps * std + mu
        else:
            return mu

    def encode(self, x):
        '''
        output - mean and logvar of z
        '''
        mu_logvar = self.q_z_x(x)
        mu, logvar = mu_logvar[:, :self.latent_dim], mu_logvar[:, :self.latent_dim]
        self.mu = mu
        self.logvar = logvar
        return mu, logvar

    def decode(self, z):
       # outputs the parameters of the likelihood
        return self.p_x_z(z)

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        self.pxz_distribution.param = self.decode(z)
        return self.pxz_distribution.param
    

    def kld(self):
        return 0.5 * torch.sum(
                -1 - self.logvar + self.mu.pow(2) + self.logvar.exp(),
            dim=(1)
        )

    def loss_function(self, x):
        rec_term = self.pxz_distribution.llkh(x)
        rec_term = torch.mean(rec_term) # average on batch dim
        kld = torch.mean(self.kld()) # average on batches dim

        L = (rec_term - self.beta * kld)

        loss = L

        loss_dict = {
            'loss': loss,
            'rec_term': rec_term,
            '-beta*kld': self.beta * kld
        }

        return loss, loss_dict

    def step(self, x):
        self.forward(x)
        loss, loss_dict = self.loss_function(x)
        rec_x = self.pxz_distribution.mean()
        return loss, rec_x, loss_dict

    def train_one_epoch(self, train_loader, device, optimizer, epoch):

        self.train()
        train_loss = 0

        for batch_idx, batch in enumerate(train_loader):
            x, lbl = batch.X, batch.obs['cell_type']
            x = x.to(device).float()
            #lbl = lbl.to(device) 
            optimizer.zero_grad() # otherwise grads accumulate in backward
                                # moreover at init it is random values

            loss, rec_x , loss_dict = self.step(
                x
            )

            (-loss).backward()
            train_loss += loss.item()
            optimizer.step()
            
        nb_mb_it = (len(train_loader.dataset) // x.shape[0])
        train_loss /= nb_mb_it
        return train_loss, x, rec_x, 0 #lbl
    
    def train_(self, train_loader, device, optimizer, nb_epochs):
        loss_val = []
        for epoch in range(nb_epochs):
            loss, x, rec_x, lbl = self.train_one_epoch(
                train_loader=train_loader,
                device=device,
                optimizer=optimizer,
                epoch=epoch)

            loss_val.append(loss)
            if (epoch + 1) % 100 == 0 or epoch == 0:
                print('epoch [{}/{}], train loss: {:.4f}'.format(
                    epoch + 1, nb_epochs, loss), flush=True)
            
        return loss_val