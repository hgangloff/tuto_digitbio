__NOTE:__ This is an archived repository, the updated and extended version can be found at [this link](https://forgemia.inra.fr/mia-paris/formations/vae_happyr_2023)

# Cell identification from scRNA-seq data with Variational Autoencoders

![VAE for scRNA-seq data](vae_main.png)

This is the repository containing all the materials for the tutorial on VAE for
scRNA-seq data presented on December, 8 2022 at the Digit-Bio seminar.

To get started check first ```install_instructions_tuto_digitbio.pdf```, then
open the notebook along with ```accompanying_slides_tuto_digitbio.pdf```.
