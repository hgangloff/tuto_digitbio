{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1b45101c",
   "metadata": {},
   "source": [
    "# Cell identification from scRNA-seq data with Variational Autoencoders\n",
    "_Hugo Gangloff - Séminaire DIGIT-BIO, 8 décembre 2022_"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f442c50",
   "metadata": {},
   "source": [
    "In this tutorial, we will study the problem of cell identification from scRNA-seq data using Variational Autoencoders (VAEs) as a tool for unsupervised dimensionality reduction. This code uses:\n",
    "- the $\\texttt{Pytorch}$ library for the deep learning related modules.\n",
    "- the Python libraries $\\texttt{scanpy}$ and $\\texttt{anndata}$ for the manipulation and processing of scRNA-seq data. An introduction to $\\texttt{anndata}$ can be found [here](https://anndata-tutorials.readthedocs.io/en/latest/getting-started.html). There exists a [scanpy tutorial](https://scanpy-tutorials.readthedocs.io/en/latest/pbmc3k.html) for cell identification from scRNA-seq data, without the deep learning part."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7bec017",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a831c6b",
   "metadata": {},
   "source": [
    "## Loading data and preprocessing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f078f33e",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scanpy as sc\n",
    "import anndata as ad\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89be4c7e",
   "metadata": {},
   "source": [
    "In the next cell, we load the dataset from our custom dataset class. We get an ```AnnData``` object (called ```adata``` in this tutorial) which corresponds to a count matrix with enhanced features. Core properties of ```AnnData``` objects are:\n",
    "- the actual cont matrix with dimensions ```n_obs``` x ```n_vars```\n",
    "- metadata aligned with the obs axis\n",
    "- metadata aligned with the var axis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "801893e3",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from data import datasets\n",
    "\n",
    "adata = datasets.MousePancreas().get_adata(preprocessing=True)\n",
    "dataset_name = \"mouse\"\n",
    "\n",
    "print(\"\\n\\n Summary of the loadad anndata matrix\", adata)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea47aa99",
   "metadata": {},
   "source": [
    "Have a look at the data: visualize a part of the matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae378534",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "print(adata[20:30,25:30].to_df())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d5f4b5cd",
   "metadata": {},
   "source": [
    "Have a look at the metadata ```cell_type``` aligned with the obs axis (the cells). This corresponds to the ground truth of our clustering we will use later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "664370b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(adata.obs[\"cell_type\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a4484e8",
   "metadata": {},
   "source": [
    "We also have the ```n_genes``` metadata, aligned with the obs axis which count the number of genes for a given cell, and the ```n_cells``` metadata, aligned with the var axis which count the number of cells for a given gene."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0e2cb68",
   "metadata": {},
   "source": [
    "__Note__: for the Continuous Bernoulli likelihood, normalization is needed since we want data in $[0, 1]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09f4bc76",
   "metadata": {},
   "outputs": [],
   "source": [
    "adata_normalized = adata.copy()\n",
    "adata_normalized.X = adata_normalized.X / np.sum(adata_normalized.X)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac5012f2",
   "metadata": {},
   "source": [
    "## Load the VAE and likelihood objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58edf2dd",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from models.vae import VAE\n",
    "from models.distributions.nb import NegativeBinomial\n",
    "from models.distributions.cb import ContinuousBernoulli\n",
    "from models.distributions.zinb import ZeroInflatedNegativeBinomial"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64797ccc",
   "metadata": {},
   "source": [
    "We set up the device for tensor computation. By default, the device is GPU if there is one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8ba4013",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "device = torch.device(\n",
    "    \"cuda\" if torch.cuda.is_available() else \"cpu\"\n",
    ")\n",
    "print(\"Cuda available ?\", torch.cuda.is_available())\n",
    "print(\"Pytorch device:\", device)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e83a7dbe",
   "metadata": {},
   "source": [
    "Then, directly from the count matrix, we create an ```AnnLoader``` object with the dedicated function from $\\texttt{anndata}$ library. These objects are iterable returning minibatches of data (in the form of $\\texttt{Pytorch}$ tensors) for the stochastic gradient descent algorithm. Thus when iterating over this object, we get batches of rows of ```adata``` along with their associated metadata. \n",
    "\n",
    "```AnnLoader``` objects are similar to the $\\texttt{Pytorch}$ ```Dataloader``` objects which are created from ```Dataset``` objects.\n",
    "\n",
    "We choose to not ```use_cuda``` by default for the data it then requires to move explicitly the data on the device during training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "159fa350",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from anndata.experimental.pytorch import AnnLoader\n",
    "dataloader = AnnLoader(adata,\n",
    "                       batch_size=256,\n",
    "                       shuffle=True,\n",
    "                       use_cuda=False\n",
    "                    )\n",
    "\n",
    "dataloader_normalized = AnnLoader(adata_normalized,\n",
    "                       batch_size=256,\n",
    "                       shuffle=True,\n",
    "                       use_cuda=False\n",
    "                    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "067718a8",
   "metadata": {},
   "source": [
    "## Training the VAE model\n",
    "We now go through the training of a VAE model with Negative Binomial likelihood, step by step."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b0b1e03",
   "metadata": {},
   "source": [
    "### Step 1: Define the hyperparameters\n",
    "They consist of:\n",
    "- a list defining the number of hidden layers in the encoder and the decoder (we have a symmetrical architecture), as well as their size\n",
    "- the size of the latent space\n",
    "- the learning rate used in the stochastic gradient descent algorithm\n",
    "- the number of epochs (_i.e._ the number of complete passes through the dataset: at each epoch we go through the entire ```AnnLoader``` iterator)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4771ff8",
   "metadata": {},
   "outputs": [],
   "source": [
    "hidden_layer_list = [32]\n",
    "latent_dim = 10\n",
    "learning_rate = 1e-3\n",
    "nb_epochs = 100"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83fc8ded",
   "metadata": {},
   "source": [
    "### Step 2: Create a VAE model with a Negative Binomial likelihood\n",
    "After the initialization, the model is moved to the chosen device for the computations (by default, at initialization it is on CPU)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b3c63e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_vae_nb = VAE(\n",
    "    input_dim=adata.n_vars,\n",
    "    hidden_dims=hidden_layer_list,\n",
    "    latent_dim=latent_dim,\n",
    "    pxz_distribution=NegativeBinomial\n",
    ")\n",
    "my_vae_nb.to(device)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6972a548",
   "metadata": {},
   "source": [
    "### Step 3: Create the optimizer\n",
    "We will use the classical Adam optimizer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "edbcdccf",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = torch.optim.Adam(\n",
    "    my_vae_nb.parameters(),\n",
    "    lr=learning_rate\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dbc0b0d0",
   "metadata": {},
   "source": [
    "### Step 4: Main training loop\n",
    "We detail all the elements of this loop over the epochs. This sequence of commands is central to training models with $\\texttt{Pytorch}$, it is not specific to the VAE model\n",
    "1) Put the model in training mode, useful for parts of the model (_e.g._ Dropout layers) which behave differently between train and eval modes\n",
    "2) At each epoch, we iterate through all the dataloader\n",
    "3) From the dataloader we get minibatches of the inputs and corresponding ground truth. Note that in this unsupervised training, we have no use of the label minibatch\n",
    "4) Move the minibatch tensor to the device (problems will occur if the model device is not the same as the minibatch device) and make sure the data type is float\n",
    "5) Set the gradients of the tensors to 0 explicitly. Otherwise gradients accumulate after each backpropagation calls (and moreover, gradients may be initialized with random values)... The latter is not the behaviour we want for stochastic gradient descent\n",
    "6) Forward pass through the model (equivalent to ```vae_nb(x)```). Sets some internal parameters in the model needed in the loss computation.\n",
    "7) Compute the value of the loss function (ELBO)\n",
    "8) Compute the VAE output (reconstruction). This will not be used in our example\n",
    "9) Compute the backward pass. By default the optimizer perform stochastic gradient descent, but in our case we want to maximize the loss function (ELBO). We thus backpropagate from the opposite of the loss value\n",
    "10) Retrieve the loss value for this minibatch\n",
    "11) Perform a step of the stochastic gradient algorithm\n",
    "12) Average the loss over the minibatches\n",
    "\n",
    "__Note__: Refer to the code for VAE-specific details.\n",
    "\n",
    "\n",
    "__Note__: You will have no problem training VAEs on this dataset using a small GPU. If you have only have a CPU and are struggling training the model, restrain yourself to a small VAE and / or short training (update the hyperparameters). But you can always stop the running cell and jump to the rest of the tutorial, we provided you with pretrained models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aecceefd",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Start training my VAE-NB model...\")\n",
    "loss_my_vae_nb = []\n",
    "\n",
    "for epoch in range(nb_epochs):\n",
    "\n",
    "    my_vae_nb.train() # 1)\n",
    "    train_loss = 0\n",
    "\n",
    "    for batch_idx, batch in enumerate(dataloader): # 2)\n",
    "        x, label = batch.X, batch.obs['cell_type'] # 3)\n",
    "        x = x.to(device).float() # 4)\n",
    "        optimizer.zero_grad() # 5)\n",
    "\n",
    "        my_vae_nb.forward(x) # 6)\n",
    "        loss, _ = my_vae_nb.loss_function(x) # 7)\n",
    "        rec_x = my_vae_nb.pxz_distribution.mean() # 8)\n",
    "\n",
    "        (-loss).backward() # 9)\n",
    "        train_loss += loss.item() # 10)\n",
    "        optimizer.step() # 11)\n",
    "        \n",
    "    nb_mb_it = (len(dataloader.dataset) // x.shape[0])\n",
    "    train_loss /= nb_mb_it # 12)\n",
    "\n",
    "    loss_my_vae_nb.append(train_loss)\n",
    "    \n",
    "    if (epoch + 1) % 10 == 0 or epoch == 0:\n",
    "        print('epoch [{}/{}], train loss: {:.4f}'.format(\n",
    "            epoch + 1, nb_epochs, loss)\n",
    "        )\n",
    "print(\"Ended training\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "311ad731",
   "metadata": {},
   "source": [
    "For code organization:\n",
    "- Steps 1) to 12) above are factorized in the model method ```train_one_step(...)``` which will be used later as:\n",
    "```python\n",
    "    loss, x, rec_x, lbl = vae_nb.train_one_epoch(\n",
    "        train_loader=dataloader,\n",
    "        device=device,\n",
    "        optimizer=optimizer,\n",
    "        epoch=epoch\n",
    "    )\n",
    "```\n",
    "\n",
    "- Steps 6) to 8) are further factorized in the model method ```step(...)``` which will be used later as:\n",
    "```python\n",
    "    loss, rec_x, _ = self.step(\n",
    "        x\n",
    "    )\n",
    "```\n",
    "\n",
    "- Finally ```train_one_step(...)``` will be factorized in the ```train_(...)``` method."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a2e01db",
   "metadata": {},
   "source": [
    "#### Display the training loss over the epochs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed0fe06b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.plot(loss_my_vae_nb)\n",
    "plt.xlabel('Epochs')\n",
    "plt.ylabel('Loss value')\n",
    "plt.title('Loss value over the epochs of the training')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de5d9403",
   "metadata": {},
   "source": [
    "#### Note on training\n",
    "It might take time to find a working combination of the hyperparameters (model architecture, learning rate and number of epochs). Some ideas for debugging:\n",
    "- ```nan``` values in loss function $\\rightarrow$ learning rate too big / error is loss function\n",
    "- loss function never converges $\\rightarrow$ learning rate too big / too small\n",
    "- model results show poor performance $\\rightarrow$ training has not converged / network too big / network too small"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1d3e59e",
   "metadata": {},
   "source": [
    "### Step 5: Save the model parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93c42808",
   "metadata": {},
   "outputs": [],
   "source": [
    "torch.save(\n",
    "    my_vae_nb.state_dict(),\n",
    "    os.path.join(\n",
    "        \"saved_model_parameters\", f\"my_vae_nb_{epoch + 1}_{dataset_name}.pth\"\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0353b097",
   "metadata": {},
   "source": [
    "## Other (pretrained) VAE models\n",
    "In order to compare models and analyze results, we now directly load some other VAE models that have been pretrained on GPU with hyperparameter fine-tuning. Leave ```mode = \"load\"``` and the hyperparameters as they are below to load. Note that ```mode = \"train\"``` has been used to train those models, so you might reuse the commands from the training mode for your future trainings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a596bb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "mode = \"load\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f1e8cbb5",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "hidden_layer_list = [128, 128]\n",
    "latent_dim = 50\n",
    "learning_rate = 1e-3\n",
    "nb_epochs = 2500\n",
    "file_to_load = f\"{nb_epochs}_{dataset_name}\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "206373b1",
   "metadata": {},
   "source": [
    "### Load a pretrained VAE with Continuous Bernoulli likelihood"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4453761a",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vae_cb = VAE(input_dim=adata_normalized.n_vars,\n",
    "            hidden_dims=hidden_layer_list,\n",
    "            latent_dim=latent_dim,\n",
    "            pxz_distribution=ContinuousBernoulli\n",
    "            )\n",
    "\n",
    "# Move the model to the device (GPU if there is one)\n",
    "vae_cb.to(device)\n",
    "\n",
    "if mode == \"load\":\n",
    "    state_dict = torch.load(\n",
    "            os.path.join(\"saved_model_parameters\", f\"vae_cb_{file_to_load}.pth\"),\n",
    "            map_location=device\n",
    "        )\n",
    "    vae_cb.load_state_dict(state_dict, strict=False)\n",
    "\n",
    "if mode == \"train\":\n",
    "    # Initialize the optimizer\n",
    "    optimizer = torch.optim.Adam(\n",
    "                    vae_cb.parameters(),\n",
    "                    lr=learning_rate\n",
    "                )\n",
    "\n",
    "    \n",
    "    print(\"Start training the VAE-CB model...\")\n",
    "    vae_cb.train_(\n",
    "        dataloader_normalized, \n",
    "        device, \n",
    "        optimizer,\n",
    "        nb_epochs\n",
    "    )\n",
    "            \n",
    "    torch.save(vae_cb.state_dict(), os.path.join(\n",
    "                        \"saved_model_parameters\", f\"vae_cb_{nb_epochs}_{dataset_name}.pth\"\n",
    "                        )\n",
    "                    )\n",
    "    print(\"Ended training and saved parameters\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41fdcf70",
   "metadata": {},
   "source": [
    "### Load a pretrained VAE with Negative Binomial likelihood"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73645259",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vae_nb = VAE(input_dim=adata.n_vars,\n",
    "            hidden_dims=hidden_layer_list,\n",
    "            latent_dim=latent_dim,\n",
    "            pxz_distribution=NegativeBinomial\n",
    "            )\n",
    "\n",
    "# Move the model to the device (GPU if there is one)\n",
    "vae_nb.to(device)\n",
    "\n",
    "if mode == \"load\":\n",
    "    state_dict = torch.load(\n",
    "            os.path.join(\"saved_model_parameters\", f\"vae_nb_{file_to_load}.pth\"),\n",
    "            map_location=device\n",
    "        )\n",
    "    vae_nb.load_state_dict(state_dict, strict=False)\n",
    "\n",
    "if mode == \"train\":\n",
    "    # Initialize the optimizer\n",
    "    optimizer = torch.optim.Adam(\n",
    "                    vae_nb.parameters(),\n",
    "                    lr=learning_rate\n",
    "                )\n",
    "\n",
    "    print(\"Start training the VAE-NB model...\")\n",
    "    \n",
    "    vae_nb.train_(\n",
    "        dataloader, \n",
    "        device, \n",
    "        optimizer,\n",
    "        nb_epochs\n",
    "    )\n",
    "        \n",
    "    torch.save(vae_nb.state_dict(), os.path.join(\n",
    "                        \"saved_model_parameters\", f\"vae_nb_{nb_epochs}_{dataset_name}.pth\"\n",
    "                        )\n",
    "                    )\n",
    "    print(\"Ended training and saved parameters\")    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d13c6e7f",
   "metadata": {},
   "source": [
    "### Load a pretrained VAE with Zero Inflated Negative Binomial likelihood"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1437dde1",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vae_zinb = VAE(input_dim=adata.n_vars,\n",
    "            hidden_dims=hidden_layer_list,\n",
    "            latent_dim=latent_dim,\n",
    "            pxz_distribution=ZeroInflatedNegativeBinomial\n",
    "            )\n",
    "vae_zinb.to(device)\n",
    "\n",
    "if mode == \"load\":\n",
    "    state_dict = torch.load(\n",
    "                os.path.join(\"saved_model_parameters\", f\"vae_zinb_{file_to_load}.pth\"),\n",
    "                map_location=device\n",
    "            )\n",
    "    vae_zinb.load_state_dict(state_dict, strict=False)\n",
    "\n",
    "if mode == \"train\":\n",
    "    optimizer = torch.optim.Adam(\n",
    "                    vae_zinb.parameters(),\n",
    "                    lr=learning_rate\n",
    "                )\n",
    "\n",
    "    print(\"Start training the VAE-ZINB model...\")\n",
    "    \n",
    "    vae_zinb.train_(\n",
    "        dataloader, \n",
    "        device, \n",
    "        optimizer,\n",
    "        nb_epochs\n",
    "    )\n",
    "    \n",
    "    torch.save(vae_zinb.state_dict(), os.path.join(\n",
    "                        \"saved_model_parameters\", f\"vae_zinb_{nb_epochs}_{dataset_name}.pth\"\n",
    "                        )\n",
    "                    )\n",
    "    print(\"Ended training and saved parameters\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c57e7788",
   "metadata": {},
   "source": [
    "## Evaluation of the models"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20ec70df",
   "metadata": {},
   "source": [
    "### Visualization of the latent spaces\n",
    "We now want to visualize $\\mu_m, \\forall m\\in\\{1,\\dots,M\\}$.\n",
    "\n",
    "We first compute all the $\\mu_m$ with the trained models. This requires to feed all the $x_m$ in the encoder network. This computation can not be done on GPU because of memory constraint. We thus move the model to the CPU.\n",
    "\n",
    "Since $\\mu_m\\in\\mathbb{R}^p$ with $p>2$, we need an additional dimension reduction approach provided by $\\texttt{sc.tl.umap}$ to visualize the sample cells $x_m$ in a 2D plane. The latter relies on the precomputation of a neighborhood map with $\\texttt{sc.tl.neighbors}$. Plotting is done with $\\texttt{sc.pl.umap}$. UMAP is a recent algorithm for dimension reduction introduced in [this](https://arxiv.org/pdf/1802.03426.pdf) paper.\n",
    "\n",
    "**Note:** The following visualizations use the ground truth cell type as colormap. In this tutorial, we do not cover the clustering step of the data in the smaller subspace.\n",
    "\n",
    "The following cell are utility functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d69612e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# create the figure that will store the visualizations\n",
    "latent_space_fig, latent_space_axes = plt.subplots(\n",
    "    nrows=2,\n",
    "    ncols=3,\n",
    "    figsize=(9, 9),\n",
    ")\n",
    "\n",
    "def dim_red(data, ax, title, legend=None, rep=None):\n",
    "    '''\n",
    "    Wrap up the different steps needed to produce a dimension reduction and its visualization\n",
    "    in scanpy\n",
    "    '''\n",
    "    sc.pp.neighbors(data, use_rep=rep)\n",
    "    sc.tl.umap(data)\n",
    "    ret_ax = sc.pl.umap(data,\n",
    "           color=['cell_type'], \n",
    "           show=False,\n",
    "           ax=ax,\n",
    "           title=title,\n",
    "           legend_loc=None if legend is None else 'right margin'\n",
    "        )\n",
    "    if legend is not None:\n",
    "        handles, labels = ret_ax.get_legend_handles_labels()\n",
    "        ret_ax.get_legend().remove()\n",
    "        # we remove the legend from the axis after having taken the materials needed to\n",
    "        # reconstruct it for the entire figure\n",
    "        return data, (handles, labels)\n",
    "\n",
    "    return data, None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "294ff845",
   "metadata": {},
   "source": [
    "The 5 following cells perform the dimension reduction and plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "584f41fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dimension reduction on raw data\n",
    "adata, _ = dim_red(adata, latent_space_axes[0, 0], 'Dimension reduction on raw data')\n",
    "X_umap_raw_data = adata.obsm[\"X_umap\"].copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b7b627f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_vae_nb.to('cpu') # because this computation wouldn't fit on the GPU\n",
    "z_my_vae_nb = my_vae_nb.encode(dataloader.dataset[:].X)[0]\n",
    "adata.obsm['z_my_vae_nb'] = z_my_vae_nb.data.cpu().numpy()\n",
    "adata, _ = dim_red(adata, latent_space_axes[0, 2], 'Latent space of my trained VAE-NB', rep='z_my_vae_nb')\n",
    "z_my_vae_nb_umap = adata.obsm[\"X_umap\"].copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54ccb2b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "vae_cb.to('cpu') # because this computation wouldn't fit on the GPU\n",
    "z_vae_cb = vae_cb.encode(dataloader_normalized.dataset[:].X.float())[0]\n",
    "adata_normalized.obsm['z_vae_cb'] = z_vae_cb.data.cpu().numpy()\n",
    "adata_normalized, _ = dim_red(adata_normalized, latent_space_axes[0, 1], 'Latent space of VAE-CB', rep='z_vae_cb')\n",
    "z_vae_cb_umap = adata_normalized.obsm[\"X_umap\"].copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5c7cab3",
   "metadata": {},
   "outputs": [],
   "source": [
    "vae_nb.to('cpu') # because this computation wouldn't fit on the GPU\n",
    "z_vae_nb = vae_nb.encode(dataloader.dataset[:].X.float())[0]\n",
    "adata.obsm['z_vae_nb'] = z_vae_nb.data.cpu().numpy()\n",
    "adata, _ = dim_red(adata, latent_space_axes[1, 0], 'Latent space of VAE-NB', rep='z_vae_nb')\n",
    "z_vae_nb_umap = adata.obsm[\"X_umap\"].copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f57d242",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "vae_zinb.to('cpu') # because this computation wouldn't fit on the GPU\n",
    "z_vae_zinb = vae_zinb.encode(dataloader.dataset[:].X.float())[0]\n",
    "adata.obsm['z_vae_zinb'] = z_vae_zinb.data.cpu().numpy()\n",
    "adata, (handles, labels) = dim_red(adata, latent_space_axes[1, 1], 'Latent space of VAE-ZINB', rep='z_vae_zinb', legend='right margin')\n",
    "z_vae_zinb_umap = adata.obsm[\"X_umap\"].copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b8beffe",
   "metadata": {},
   "outputs": [],
   "source": [
    "latent_space_fig.legend(handles, labels, loc='right', bbox_to_anchor=(0.95, 0.27))\n",
    "latent_space_fig.delaxes(latent_space_axes[1, 2])\n",
    "latent_space_fig.tight_layout()\n",
    "latent_space_fig"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a7d7ece",
   "metadata": {},
   "source": [
    "**Conclusions ?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a3738546",
   "metadata": {},
   "source": [
    "### Average silhouette width (AWS)\n",
    "We now try to have a quantitative result with the AWS metric that corroborates the previous qualitative conclusions. Note that the AWS metric requires the true cell type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16d202ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import silhouette_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb458102",
   "metadata": {},
   "outputs": [],
   "source": [
    "s_score_raw = silhouette_score(adata.X, adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score of raw data is\", s_score_raw)\n",
    "s_score_raw_umap = silhouette_score(X_umap_raw_data, adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score of raw data after Umap is\", s_score_raw_umap)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76ed6b20",
   "metadata": {},
   "outputs": [],
   "source": [
    "s_score_cb = silhouette_score(z_vae_cb.data.cpu().numpy(), adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-CB is\", s_score_cb)\n",
    "s_score_cb_umap = silhouette_score(z_vae_cb_umap, adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-CB after Umap is\", s_score_cb_umap)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c576384c",
   "metadata": {},
   "outputs": [],
   "source": [
    "s_score_nb = silhouette_score(z_vae_nb.data.cpu().numpy(), adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-NB is\", s_score_nb)\n",
    "s_score_nb_umap = silhouette_score(z_vae_nb_umap, adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-NB after Umap is\", s_score_nb_umap)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45b3e55b",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "s_score_zinb = silhouette_score(z_vae_zinb.data.cpu().numpy(), adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-ZINB is\", s_score_zinb)\n",
    "s_score_zinb_umap = silhouette_score(z_vae_zinb_umap, adata.obs[\"cell_type\"])\n",
    "print(\"The silhouette score for VAE-ZINB after Umap is\", s_score_zinb_umap)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c656bd59",
   "metadata": {},
   "source": [
    "**Note:** It can be important to compute the AWS score both before (if computationally feasible) and after the dimension reduction in order to make sure that the dimension reduction algorithm is not the major cause for the structure we end up with in reduced dimension.\n",
    "\n",
    "**Conclusions ?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c66b112",
   "metadata": {},
   "source": [
    "### Latent dimension activity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "281c9274",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "latent_space_cov_fig, latent_space_cov_axes = plt.subplots(\n",
    "    nrows=1,\n",
    "    ncols=3,\n",
    "    subplot_kw=dict(aspect=\"equal\"),\n",
    "    figsize=(13, 9)\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb54f1ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "latent_space_cov_cb = np.cov(z_vae_cb.data.cpu().numpy().T)\n",
    "mat = latent_space_cov_axes[0].matshow(latent_space_cov_cb)\n",
    "latent_space_cov_axes[0].set_title(\"Latent space cov matrix of VAE-CB\")\n",
    "plt.colorbar(mat, ax=latent_space_cov_axes[0], fraction=0.046, pad=0.04)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d0d74ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "latent_space_cov_nb = np.cov(z_vae_nb.data.cpu().numpy().T)\n",
    "mat = latent_space_cov_axes[1].matshow(latent_space_cov_nb)\n",
    "latent_space_cov_axes[1].set_title(\"Latent space cov matrix of VAE-NB\")\n",
    "plt.colorbar(mat, ax=latent_space_cov_axes[1], fraction=0.046, pad=0.04)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2eb54b97",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "latent_space_cov_zinb = np.cov(z_vae_zinb.data.cpu().numpy().T)\n",
    "mat = latent_space_cov_axes[2].matshow(latent_space_cov_zinb)\n",
    "latent_space_cov_axes[2].set_title(\"Latent space cov matrix of VAE-ZINB\")\n",
    "plt.colorbar(mat, ax=latent_space_cov_axes[2], fraction=0.046, pad=0.04)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50141dbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "latent_space_cov_fig.tight_layout()\n",
    "latent_space_cov_fig"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1283aa0",
   "metadata": {},
   "source": [
    "**Conclusions ?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "236d552e",
   "metadata": {},
   "source": [
    "### Latent dimension activity\n",
    "Compute the latent random variable activity score. Recall that a latent random variable $z_{m,p}$ is considered active if $A_{z_{m,p}}>0.01$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c84e07b",
   "metadata": {},
   "outputs": [],
   "source": [
    "activity_cb = np.diag(latent_space_cov_cb)\n",
    "print(\"Number of active dimensions in VAE-CB:\", np.count_nonzero(activity_cb > 1e-2), \"/\", vae_cb.latent_dim)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10d20b23",
   "metadata": {},
   "outputs": [],
   "source": [
    "activity_nb = np.diag(latent_space_cov_nb)\n",
    "print(\"Number of active dimensions in VAE-NB:\", np.count_nonzero(activity_nb > 1e-2), \"/\", vae_nb.latent_dim)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81cd72b1",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "activity_zinb = np.diag(latent_space_cov_zinb)\n",
    "print(\"Number of active dimensions in VAE-ZINB:\", np.count_nonzero(activity_zinb > 1e-2), \"/\", vae_zinb.latent_dim)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "914a5086",
   "metadata": {},
   "source": [
    "**Conclusions ?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f7a6b18",
   "metadata": {},
   "source": [
    "# To go further...\n",
    "To go further, reproduce the above steps to perform the same experiment of cell identification in the latent space on the PBMC dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f219be5",
   "metadata": {},
   "source": [
    "Start by loading the PBMC dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "959d6577",
   "metadata": {},
   "outputs": [],
   "source": [
    "from data import datasets\n",
    "\n",
    "adata = datasets.PBMC().get_adata(subsample=0.1, preprocessing=True)\n",
    "dataset_name = \"pbmc\"\n",
    "\n",
    "print(\"\\n\\n Summary of the loadad anndata matrix\", adata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d478aa61",
   "metadata": {},
   "outputs": [],
   "source": [
    "adata_normalized = adata.copy()\n",
    "adata_normalized.X = adata_normalized.X / np.sum(adata_normalized.X)\n",
    "\n",
    "from anndata.experimental.pytorch import AnnLoader\n",
    "\n",
    "dataloader = AnnLoader(adata,\n",
    "                       batch_size=256,\n",
    "                       shuffle=True,\n",
    "                       use_cuda=False\n",
    "                    )\n",
    "\n",
    "dataloader_normalized = AnnLoader(adata_normalized,\n",
    "                       batch_size=256,\n",
    "                       shuffle=True,\n",
    "                       use_cuda=False\n",
    "                    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f26a606",
   "metadata": {},
   "source": [
    "Set up your hyperparameters, define the model, train the network, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d85d862b",
   "metadata": {},
   "source": [
    "__Note__: This dataset is much bigger and training requires a GPU. That is why we provided you with pretrained models which you can load using the hyperparameters:\n",
    "```python\n",
    "    hidden_layer_list = [64, 64]\n",
    "    latent_dim = 50\n",
    "    learning_rate = 1e-4\n",
    "    nb_epochs = 500\n",
    "    file_to_load = f\"{nb_epochs}_{dataset_name}\"\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "106f0c8d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.15"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
